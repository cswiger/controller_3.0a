/*
 *  Notes on my hack of Controller_3.0
 *  12/9/2017 - got it working with my physical plant - with the 4A Seeed motor shield could not use int pin 12 ??
 *  Switched to pin 3 and commented out attachInterrupt for the right motor encoder in Setup.
 *  
 *  Controller_3.0a - changed to normal interrupt on pin 3, instead of PinChangeInt
 */


// Library Inclusions

#include <I2Cdev.h>                             // IMU IC2 Communication
#include <Wire.h>                               // IMU IC2 Communication
#include "MPU6050_6Axis_MotionApps20_mod_1.h"   // IMU,  changed value in line 305 to 1 to change update speed from every 15ms to every 10ms
#include <PinChangeInt.h>                       // Pin Change Interrupt
#include <digitalWriteFast.h>                   // Fast Digital Readings



// Pin Assignments

#define   pin_left_PMW          9
#define   pin_left_EncoderA     2
#define   pin_left_EncoderB     A2

#define   pin_right_PMW         10
#define   pin_right_EncoderA    3    
#define   pin_right_EncoderB    4

// These changed to match the Seeed Studio 2 & 4A motor board - had a 2Amp board from Radio Shack long ago, and just ordered a 4A from an Amazon vendor
                                                      // but it's broken :(
// got refund for the busted one and yet another one direct from Seeed - that works
// Actually the 4A board pins do match the schematic so setting back
#define   pin_IN1               5	// 8 on the 2A board
#define   pin_IN2               6	// 11 on the 2A board
#define   pin_IN3               7	// 12 on the 2A board
#define   pin_IN4               8	// 13 on the 2A board

#define   pin_left_Current      A0
#define   pin_right_Current     A1

#define   pin_IMU_Interrupt     3   // try 3 and disable interrupt 1  --- This works!! Commented out attach interrupt for right motor encoder in Setup
#define   pin_IMU_SDA           A4
#define   pin_IMU_SCL           A5

#define   pin_Pot               A3
#define   pin_Pot_kp            //
#define   pin_Pot_ki            //
#define   pin_Pot_kd            //



// Interrupt Assignment 

// Do these correspond to the pin_x_Encoder_A above on enterrupt enabled pins 2,3 above? Would make sense 
#define   interruptPin_left_Encoder    0
#define   interruptPin_right_Encoder   1
